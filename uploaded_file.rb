# uploaded_file.rb
require "aws-sdk-s3"
require "mini_magick"
require 'fileutils'

class UploadedFile
    def self.from_s3(bucket_name, object_name)
        s3 = Aws::S3::Resource.new()
        object = s3.bucket(bucket_name).object(object_name)
        folderName = object_name.split("/")[0]
        FileUtils.mkdir_p "/tmp/#{folderName}"
        tmp_file_name = "/tmp/#{object_name}"
        object.get(response_target: tmp_file_name)
        
        UploadedFile.new(tmp_file_name)
    end

    def initialize(tmp_file)
        @tmp_file = tmp_file
    end

    def resize(resizingWidth, resizingHeight)
        image = MiniMagick::Image.open(@tmp_file)
        imageWidth = image.width.to_f
        imageHeight = image.height
        aspectRatio = imageWidth / imageHeight
        aspectRatio = aspectRatio.round(2)
        resizedImageWidth = resizingWidth
        resizedImageHeight = resizedImageWidth.to_f / aspectRatio
        resizingParams = resizedImageWidth.to_s + "x" + resizedImageHeight.to_s
        resizedImage = image.resize(resizingParams)
        resizedImage.strip
        resizedImage.quality(40)

        @resized_tmp_file = "/tmp/resized.jpg"
        resizedImage.write @resized_tmp_file
    end
    
    # resizeAndCrop final function: will give crop as final result
    def resizeAndCrop(cropWidth, cropHeight)
        puts "new test"
        image = MiniMagick::Image.open(@tmp_file)
        imageWidth = image.width.to_f
        imageHeight = image.height
        aspectRatio = imageWidth / imageHeight
        cropWidth = cropWidth.to_i
        cropHeight = cropHeight.to_i
        cropAspectRatio = cropWidth.to_f / cropHeight

        if aspectRatio > 1
            if cropAspectRatio > 1
                #resize the image:
                resizingWidth = cropWidth
                resizingHeight = cropWidth / aspectRatio
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                resizedImageWidth = resizedImage.width.to_f
                resizedImageHeight = resizedImage.height

                #crop the image:
                cropHeightPercentage = (cropHeight / resizedImageHeight.to_f) * 100
                cropYOffset = (resizedImageHeight - cropHeight) / 2.0
                cropParams = "100%x" + cropHeightPercentage.to_s + "%+0+" + cropYOffset.to_s
            elsif cropAspectRatio <= 1
                #resize the image:
                resizingHeight = cropHeight
                resizingWidth = (cropHeight * aspectRatio).to_i
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                resizedImageWidth = resizedImage.width.to_f
                resizedImageHeight = resizedImage.height

                #crop the image:
                cropWidthPercentage = (cropWidth / resizedImageWidth.to_f) * 100
                cropXOffset = (resizedImageWidth - cropWidth) / 2.0
                cropParams = cropWidthPercentage.to_s + "%x100%+" + cropXOffset.to_s + "+0"                    
            end

            croppedImage = resizedImage.crop(cropParams)                
        elsif aspectRatio < 1
            if cropAspectRatio >= 1
                #resize the image:
                resizingWidth = cropWidth
                resizingHeight = cropWidth / aspectRatio
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                resizedImageWidth = resizedImage.width.to_f
                resizedImageHeight = resizedImage.height

                #crop the image:
                cropHeightPercentage = (cropHeight / resizedImageHeight.to_f) * 100
                cropYOffset = (resizedImageHeight - cropHeight) / 2.0
                cropParams = "100%x" + cropHeightPercentage.to_s + "%+0+" + cropYOffset.to_s
            elsif cropAspectRatio < 1
                #resize the image:
                resizingHeight = cropHeight
                resizingWidth = cropHeight * aspectRatio
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                resizedImageWidth = resizedImage.width.to_f
                resizedImageHeight = resizedImage.height

                #crop the image:
                cropWidthPercentage = (cropWidth / resizedImageWidth.to_f) * 100
                cropXOffset = (resizedImageWidth - cropWidth) / 2.0
                cropParams = cropWidthPercentage.to_s + "%x100%+" + cropXOffset.to_s + "+0" 
            end

            croppedImage = resizedImage.crop(cropParams)        
        elsif aspectRatio == 1
            if cropAspectRatio > 1
                #resize the image:
                resizingWidth = cropWidth
                resizingHeight = resizingWidth
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                resizedImageWidth = resizedImage.width.to_f
                resizedImageHeight = resizedImage.height

                #crop the image:
                cropHeightPercentage = (cropHeight / resizedImageHeight.to_f) * 100
                cropYOffset = (resizedImageHeight - cropHeight) / 2.0
                cropParams = "100%x" + cropHeightPercentage.to_s + "%+0+" + cropYOffset.to_s
                croppedImage = image.crop(cropParams)
            elsif cropAspectRatio < 1
                #resize the image:
                resizingHeight = cropHeight
                resizingWidth = resizingHeight
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                resizedImageWidth = resizedImage.width.to_f
                resizedImageHeight = resizedImage.height

                #crop the image:
                cropWidthPercentage = (cropWidth / resizedImageWidth.to_f) * 100
                cropXOffset = (resizedImageWidth - cropWidth) / 2.0
                cropParams = cropWidthPercentage.to_s + "%x100%+" + cropXOffset.to_s + "+0"
                croppedImage = image.crop(cropParams)
            elsif cropAspectRatio == 1
                #resize and crop the image:
                resizingWidth = cropWidth
                resizingHeight = resizingWidth
                resizingParams = resizingWidth.to_s + "x" + resizingHeight.to_s
                resizedImage = image.resize(resizingParams)
                croppedImage = resizedImage
            end              
        end

        if (aspectRatio - cropAspectRatio).abs > 1
            imageTwoResizingParams = croppedImage.width.to_s + "x" + croppedImage.height.to_s
            imageTwo = MiniMagick::Image.open(@tmp_file)
            resizedImageTwo = imageTwo.resize(imageTwoResizingParams)
            croppedImage.blur "0x3"

            geometryX = (croppedImage.width - resizedImageTwo.width) / 2.0
            geometryY = (croppedImage.height - resizedImageTwo.height) / 2.0

            croppedImage = croppedImage.composite(resizedImageTwo) do |c|
                c.compose "Over"
                c.geometry "+" + geometryX.to_s + "+" + geometryY.to_s
            end
        end    

        croppedImage.strip
        croppedImage.quality(40)
        @resized_tmp_file = "/tmp/resizedAndCropped.jpg"
        croppedImage.write @resized_tmp_file    
    end

    def upload_file(target_bucket, target_object)
        s3 = Aws::S3::Resource.new()
        object = s3.bucket(target_bucket).object(target_object).upload_file(@resized_tmp_file)
    end
end

