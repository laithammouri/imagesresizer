#handler.rb
require 'uploaded_file'
require 'JsonReader'
require 'json'

class ImageHandler
  def self.process(event:, context:)
    event = event["Records"].first
    bucket_name = event["s3"]["bucket"]["name"]
    object_name = event["s3"]["object"]["key"]

    # upload image via UploadedFile class:
    file = UploadedFile.from_s3(bucket_name, object_name)

    # upload dimensions file via JsonReader class:
    dimensionsFile = JsonReader.from_s3(bucket_name, "dimensions.json")
    result = JSON.parse(dimensionsFile.read)
    directoryName = object_name.split("/")[0]
    words = object_name.split("/")
    imageName = words[words.length-1]

    result[directoryName].each do |dimension|
        if dimension['method'] == "resize"
          file.resize(dimension['width'], dimension['height'])
          file.upload_file("resizing-trials", "#{directoryName}/resize-#{dimension['width']}-#{dimension['height']}/#{imageName}")
        elsif dimension['method'] == "crop"
          file.resizeAndCrop(dimension['width'], dimension['height'])
          file.upload_file("resizing-trials", "#{directoryName}/crop-#{dimension['width']}-#{dimension['height']}/#{imageName}")
        end
    end
  end
end